**machine-learning-algorithm:**
<br /> - run sort_iamges.py and then run model/main.py
<br /> - to enable muticlass change sort_pad() to sort_pad(muticlass=True)

**mobile-app:**
<br /> - open in Android Studio and change the API address to your backends address

**Demo:**
<br /> The demo first shows a benign classification for a benign image and then a malignant classification for a malignant image
> https://leeds365-my.sharepoint.com/:v:/g/personal/ll16d4b_leeds_ac_uk/EahkcM1fUD1HgvmZI6TaUxEB_02Btx-Jqei4hIOJD8KK8g?e=CErduc
